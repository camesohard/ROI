package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/fhs/gompd/mpd"
)

type Config struct {
	MpdServer  string
	MpdPort    int
	ServerPort int
}

type SongInfo struct {
	Album string `json:"album"`
	Title string `json:"name"`
	File  string `json:"file_name"`
}

// Catch usual boring errors
func catchErrors(err error) {
	if err != nil {
		log.Println(err)
	}
}

// Ping mpd server to keep our connection alive
// And play a song if its not already playing
func keepAlive(conn *mpd.Client) {
	if err := conn.Ping(); err != nil {
		log.Println(err)
	}

	status, err := conn.Status()
	catchErrors(err)

	if status["state"] != "playing" {
		err := conn.Play(-1)
		catchErrors(err)
	}
}

// Main page to listen to stream
func mainPage(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("../public/main_page.html")
	catchErrors(err)

	err = t.Execute(w, nil)
	catchErrors(err)
}

// Get song information
func getSongName(w http.ResponseWriter, r *http.Request, conn *mpd.Client) {
	keepAlive(conn)

	songInfo, err := conn.CurrentSong()
	catchErrors(err)

	info := SongInfo{songInfo["Album"], songInfo["Title"], songInfo["file"]}

	w.Header().Set("Content-Type", "application/json")

	err = json.NewEncoder(w).Encode(info) // Return json data
	catchErrors(err)
}

func readConfig(config *Config) {
	if _, err := toml.DecodeFile("../config/config.toml", &config); err != nil {
		log.Fatal(err)
	}
}

func main() {
	var config Config
	readConfig(&config)

	// Make first connection to the mpd server
	conn, err := mpd.Dial("tcp", fmt.Sprintf("%s:%d", config.MpdServer, config.MpdPort))
	if err != nil {
		log.Fatal(err)
	}

	keepAliveTicker := time.NewTicker(30 * time.Second)
	shuffleTicker := time.NewTicker(30 * time.Minute)

	go func() {
		for {
			select {
			case <-keepAliveTicker.C:
				// Ping server every 30 seconds
				keepAlive(conn)
			case <-shuffleTicker.C:
				// Shuffle playlist every 30 minutes to make it less boring
				log.Println("Shuffled playlist!")
				if err := conn.Shuffle(0, -1); err != nil {
					log.Println(err)
				}
			}
		}
	}()

	http.HandleFunc("/", mainPage)
	http.HandleFunc("/current", func(w http.ResponseWriter, r *http.Request) {
		getSongName(w, r, conn)
	})

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", config.ServerPort), nil))
}
