ROI
============

A simple server that lets visitors to listen what you are listening to in real time!

## Setup
Clone this repo to your desktop and build with `go build`

Edit controller configs on config/config.toml

Copy and paste example_config/mpd.conf to your original mpd config

---

## Usage
Once its built, you can run  binary file to start the server. You will then be able to access it at localhost:ServerPort

---

## License
>You can check out the full license [here](https://git.ecma.fun/ecma/ROI/src/branch/master/LICENSE)

This project is licensed under the terms of the **WTFPL** license.
